<?php get_header(); ?>
	<main role="main">
		<section id="bloco-lead">
			<div class="wrap-like-table">
				<div class="wrap-like-table-content">
					<div class="container" id="app-lead">
						<?php the_field('lead_conteudo'); ?>
						
						<form v-on:submit="enviarLead('<?php echo admin_url( 'admin-ajax.php' ); ?>', $event)" id="formlead">
							<div class="feedback">teste</div>
							<input type="email" name="email" id="email-form-lead" placeholder="seunome@seuprovedor.com.br" class="ipt-large" v-model="email">
							<button class="btn btn-vermelho btn-large" type="submit" v-bind:disabled="!isValid">Receber</button>
						</form>

						<a href="javascript: void(0);" class="btn btn-go-content scrollto" data-etargert="#bloco-release">
							<i class="fa fa-chevron-down"></i>
						</a>
					</div>
				</div>
			</div>

			<nav class="redes-sociais">
				<?php echo get_template_part('template-part-links-sociais', 'Links Sociais' ); ?>
			</nav>
		</section>
		<section id="bloco-release">
			<article>
				<div class="area-content gray">
					<div class="container">
						<?php the_field('release_introducao'); ?>
					</div>
				</div>
				<div class="area-content">
					<div class="container">
						<?php the_field('release_conteudo'); ?>
						
						<p class="text-center">
							<a href="javascript: void(0);" class="btn btn-vermelho btn-large scrollto" data-etargert="body"><i class="fa fa-chevron-up"></i> <?php the_field('release_texto_botao'); ?></a>
						</p>
					</div>

				</div>
			</article>
		</section>
		<section id="bloco-empiricus">
			<div class="container">
				<div class="row row-12">
					<div class="col col-small-12 col-med-7">
						<?php the_field('empiricus_conteudo'); ?>
						<a  href="<?php the_field('empiricus_link_botao'); ?>" target="_blank" class="btn btn-vermelho btn-medium"><?php the_field('empiricus_texto_botao'); ?></a>
					</div>
				</div>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/empricus-line.png" height="434" width="472" alt="Empiricus Research" class="float-image">
			</div>
		</section>
	</main>

	<?php get_footer(); ?>