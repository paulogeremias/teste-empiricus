<?php  

function add_assets() {
  wp_enqueue_style( 'font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css', null);
  wp_enqueue_style( 'gfont-raleway', 'https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,700,700i,900,900i', null);

  wp_enqueue_style( 'empiricus', 'http://www.paulowd.com.br/teste-empiricus/wp-content/themes/compre-dolar' .'/assets/css/style.css', null);
  wp_enqueue_script( 'vue-js', 'https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.4/vue.min.js', array(),'', false );
  wp_enqueue_script( 'vue-resource', 'https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.1.16/vue-resource.min.js', array('vue-js'),'', false );

  wp_enqueue_script( 'form-empiricus', 'http://www.paulowd.com.br/teste-empiricus/wp-content/themes/compre-dolar' .'/assets/js/form.min.js', array('vue-js', 'vue-resource'),'', true );
  wp_enqueue_script( 'empiricus-js', 'http://www.paulowd.com.br/teste-empiricus/wp-content/themes/compre-dolar' .'/assets/js/script.min.js', array(), '', true );

}
add_action( 'wp_enqueue_scripts', 'add_assets' );


add_action( 'init', 'register_cpts' );

function register_cpts() {
  $labels = array(
    'name'               => 'Leads',
    'singular_name'      => 'Lead',
    'menu_name'          => 'Leads',
    'name_admin_bar'     => 'Lead',
    'add_new'            => 'Adicionar Novo',
    'add_new_item'       => 'Adicionar Novo Lead',
    'new_item'           => 'Novo Lead',
    'edit_item'          => 'Editar Lead',
    'view_item'          => 'Visualizar Lead',
    'all_items'          => 'Todos os Leads',
    'search_items'       => 'Buscar Leads',
    'parent_item_colon'  => 'Leads pai:',
    'not_found'          => 'Nenhum lead encontrado.',
    'not_found_in_trash' => 'Nenhum lead encontrado na lixeira.'
    );

  $args = array(
    'labels'             => $labels,
    // 'description'        =>  'Description.', 'your-plugin-textdomain' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'portfolio' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => null,
    'supports'           => array( 'title', 'revisions', 'thumbnail', tags )
    );

  register_post_type( 'lead', $args );
}

// var_dump($_GET); exit;

function salvaLead(){
    $email = $_POST['email'];
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
        wp_send_json_error(array('message' => 'E-mail inválido.') );
        exit;
    }
    wp_insert_post( array(
            'post_type' => 'lead',
            'post_title' => $email,
            'post_author' => 1
        ) 
    );

    wp_send_json(array('message' => 'E-mail cadastrado com sucesso.') );
    exit;
}

add_action( 'wp_ajax_nopriv_salvalead', 'salvaLead' );
add_action( 'wp_ajax_salvalead', 'salvaLead' );

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title'    => 'Opções do Tema',
        'menu_title'    => 'Opções do Tema',
        'menu_slug'     => 'empiricus-settings',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));
}

function empiricus_init(){
  add_theme_support( 'post-thumbnails' );
    register_nav_menus( array(
        'menu_principal' => 'Menu principal localizado no topo do site',
    ) );
}

add_action( 'init', 'empiricus_init' );


/**
 * Troca do logo no admin
 */
function login_logo_mdo() {
?>
<style type="text/css">
body.login div#login h1 a {
    background-image: url(<?php bloginfo('template_url'); ?>/assets/img/logo-empiricus.png) !important;
    background-size: contain !important;
    padding-bottom: 30px;
    width: 240px !important;
    height: 60px !important;
}
</style>
 <?php
} add_action( 'login_enqueue_scripts', 'login_logo_mdo' );