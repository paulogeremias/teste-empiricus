/*** ENVIO FORM */

/*
 * Validations
 */
Vue.http.options.emulateJSON = true; // envia como form

var emailRE = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
const app = new Vue({
    el: '#app-lead',
    data: {
		email: '',
		action: 'salvalead',
		debug: true,
		// ajaxRequest: false,
    },
    methods: {
        enviarLead: function(url, event) {
            event.preventDefault();
            // jQuery('.feedback').html('Enviando...').removeClass('erro').removeClass('sucesso').addClass('loading');
            var dados = {
				email: this.email,
				action: this.action,
            };

            console.log(dados);

            this.$http.post(url, dados, function (data, status, request) {
            	var feedback = document.querySelector(".feedback");

        		if( feedback.classList.contains('erro') ){
					feedback.classList.remove('erro');
				}

        		if( feedback.classList.contains('sucesso') ){
					feedback.classList.remove('sucesso');
				}

				feedback.innerHTML = data.message;

            	if(status==200){
            		if( !feedback.classList.contains('sucesso') ){
            			document.querySelector('#email-form-lead').value = '';
						feedback.classList.add('sucesso');
					}
            	} else {
            		if( !feedback.classList.contains('erro') ){
						feedback.classList.add('erro');
					}
            	}
	        });
        }
    },
    computed: {
	    validation: function () {
	      	return {
	        	email: emailRE.test(this.email)
	      	}
	    },
	    isValid: function () {
	        var validation = this.validation
		    return Object.keys(validation).every(function (key) {
		        return validation[key]
		    })
	    }
    },
});