var w, h;

function atualizaMediaQueries(){
	w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
	h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);	
}

atualizaMediaQueries();

window.addEventListener('resize', function(){
});

window.addEventListener('scroll', function(){
	var doc = document.documentElement;
	var top = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);
	if(top > 300){
		if( !document.querySelector("#header").classList.contains('reduzido') ){
			document.querySelector("#header").classList.add('reduzido');
		}
	} else {
		if( document.querySelector("#header").classList.contains('reduzido') ){
			document.querySelector("#header").classList.remove('reduzido');
		}
	}
});

document.addEventListener('DOMContentLoaded', function(){
	var scrollToElements = document.querySelectorAll('.scrollto');

	Array.from(scrollToElements).forEach(link => {
	    link.addEventListener('click', function(event) {
	    	etargert = link.dataset.etargert;
    		y = document.querySelector(etargert).offsetTop;

	    	if(y>50)
	    		y-=50;
	    	else
	    		y=0;
			scrollTo(document.body, y, 400);
	    });
	});

	document.querySelector('#controle-menu').addEventListener('click', function(event) {
		var menu = document.querySelector('nav.main-menu');
    	if( menu.classList.contains('aberto') ){
			menu.classList.remove('aberto');
		} else {
			menu.classList.add('aberto');
		}
    });

	var menuElements = document.querySelectorAll('nav.main-menu ul li a');

	Array.from(menuElements).forEach(elemento => {
	    elemento.addEventListener('click', function(event){
	    	href = elemento.getAttribute("href"); 
	    	href = href.replace('!', '');
	    	y = document.querySelector(href).offsetTop;

	    	if(y>50)
	    		y-=50;
	    	else
	    		y=0;
			scrollTo(document.body, y, 400);
			var menu = document.querySelector('nav.main-menu');
	    	if( menu.classList.contains('aberto') ){
				menu.classList.remove('aberto');
			}
	    	return false;
	    });
	});

});

function scrollTo(element, to, duration) {
    if (duration <= 0) return;
    var difference = to - element.scrollTop;
    var perTick = difference / duration * 10;

    setTimeout(function() {
        element.scrollTop = element.scrollTop + perTick;
        if (element.scrollTop === to) return;
        scrollTo(element, to, duration - 10);
    }, 10);
}