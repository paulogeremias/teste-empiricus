var gulp = require('gulp'),
	sass = require('gulp-sass'),
	sourcemaps = require('gulp-sourcemaps');

gulp.task('styles', function () {
  	gulp.src('./assets/sass/*.scss')
	  	.pipe(sourcemaps.init())
	    .pipe(sass({ outputStyle: 'compressed' })).on('error', sass.logError)
	  	.pipe(sourcemaps.write('./maps'))
	    .pipe(gulp.dest('./assets/css'));
});

gulp.task('default', function() {
	gulp.watch('assets/sass/*.scss', ['styles']);
});