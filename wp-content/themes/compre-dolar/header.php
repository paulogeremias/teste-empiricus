<!DOCTYPE html>
<html lang="pt-br" prefix="og: http://ogp.me/ns#">
<head>
	<meta charset="UTF-8">
	<title><?php echo wp_title( '-', true, 'right' ) . get_bloginfo( 'name' ); ?></title>
	<?php wp_head(); ?>
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<meta property="og:title" content="<?php bloginfo('name'); ?>" />
	<meta property="og:type" content="article" />
	<meta property="og:url" content="<?php bloginfo('url'); ?>" />
	<meta property="og:site_name" content="Empiricus Research" />
	<meta property="og:image" content="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/share.jpg" />
	<meta property="og:image:type" content="image/jpeg" />
	<meta property="og:image:width" content="1200" />
	<meta property="og:image:height" content="630" />
</head>
<body>
	<header id="header">
		<div class="container">
			<div class="row row-15">
				<div class="col col-small-15 col-med-4">
					<a href="#" class="logo"><img src="<?php the_field('logo', 'option'); ?>" alt="Empiricus Research"></a>
				</div>
				<div class="col col-small-15 col-med-11 no-pl">
					<?php   
						$args = array(
							'theme_location' => '',
							'menu' => '',
							'container' => 'nav',
							'container_class' => 'main-menu',
							'container_id' => '',
							'menu_class' => 'menu',
							'menu_id' => '',
							'echo' => true,
							'fallback_cb' => 'wp_page_menu',
							'before' => '',
							'after' => '',
							'link_before' => '',
							'link_after' => '',
							'items_wrap' => '<ul id = "%1$s" class = "%2$s">%3$s</ul>',
							'depth' => 0,
							'walker' => ''
						);
					
						wp_nav_menu( $args ); ?>
				</div>
			</div>
		</div>
		<div id="controle-menu"><i class="fa fa-bars"></i></div>
	</header>