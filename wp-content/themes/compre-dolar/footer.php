
	<footer id="footer">
		<div class="container">
			<div class="row row-12">
				<div class="col col-lg-6 col-med-5">
					<h3>Endereço</h3>
					<p>Rua Joaquim Floriano, 913 - 4º andar<br>
						CEP: 04354-013 - Itaim Bibi. São Paulo/SP</p>
				</div>
				<div class="col col-lg-3 col-med-4">
					<h3>Redes Sociais</h3>
					<nav class="redes-sociais black small float">
						<ul>
							<?php echo get_template_part('template-part-links-sociais', 'Links Sociais' ); ?>
						</ul>
					</nav>
				</div>
				<div class="col col-lg-3 col-med-3">
					<h3>Sobre nós</h3>
					<p>Conheça mais sobre esse e outros assuntos em <a href="http://www.empiricus.com.br" target="_blank">empiricus.com.br</a></p>
				</div>
			</div>
			<div class="texto-legal">
				<?php the_field('texto_legal', 'option'); ?>
			</div>
		</div>
	</footer>
	<?php wp_footer(); ?>
</body>
</html>